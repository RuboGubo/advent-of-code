use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::Range;

use itertools::Itertools;

fn main() {
    let file = File::open("./input/part1.txt").unwrap();
    let mut reader = BufReader::new(file)
        .lines()
        .flatten();

    let seeds: Vec<u32> = reader.next().unwrap().split(" ").flat_map(str::parse::<u32>).collect();

    println!("Seeds: {seeds:?}");

    let group = reader
        .skip(1)
        .group_by(|x| x.is_empty())
        .into_iter()
        .filter(|(key, _)| !*key)
        .map(|(_key, group)| group)
        .map(process_map)
        // .sum();
        .for_each(|_| ());

    println!("{:?}", "sum")
}

fn process_map<I>(mut group: I)
where
    I: Iterator<Item = String>
{
    let heading = group.next().unwrap();

    // Generate ranges
    let fun: Vec<Vec<Range<u32>>> = group.map(|x| x.split(" ").flat_map(str::parse::<u32>).collect::<Vec<u32>>()).map(list_to_range).collect();

    println!("{fun:?}");
}

fn list_to_range(list: Vec<u32>) -> [Range<u32>; 2] {
    let destination_start = list[0];
    let source_start = list[1];
    let range = list[2];
    let destination_range = destination_start..(destination_start + range);
    let source_range = source_start..(source_start + range);
    return [destination_range, source_range];
}