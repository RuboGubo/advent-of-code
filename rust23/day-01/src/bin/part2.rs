use std::fs::File;
use std::io::{BufRead, BufReader};

use itertools::Itertools;

fn main() {
    let file = File::open("./input/part2.txt").unwrap();
    let reader = BufReader::new(file);
    
    let mut sum: u32 = 0;
    for line in reader.lines().flatten() {
        println!("New line: {line}");
        let items: Vec<u32> = (line + "    ")
            .chars()
            .tuple_windows::<(char, char, char, char, char)>()
            .map(|tuple| {
                tuple.0.to_string()
                    + &tuple.1.to_string()
                    + &tuple.2.to_string()
                    + &tuple.3.to_string()
                    + &tuple.4.to_string()
            })
            .flat_map(|word| {
                if let Some(x) = word.chars().next() {
                    if let Some(xx) = x.to_digit(10) {
                        return Some(xx);
                    } else {
                        let t = &word[0..3];
                        if t == "one" {
                            return Some(1);
                        } else if t == "two" {
                            return Some(2);
                        } else if t == "six" {
                            return Some(6);
                        }

                        let t = &word[0..4];
                        if t == "four" {
                            return Some(4);
                        } else if t == "five" {
                            return Some(5);
                        } else if t == "nine" {
                            return Some(9);
                        }

                        let t = &word[0..5];
                        if t == "three" {
                            return Some(3);
                        } else if t == "seven" {
                            return Some(7);
                        } else if t == "eight" {
                            return Some(8);
                        }
                    }
                }
                None
            })
            .inspect(|x| println!("{}", x))
            .collect();

        let a: u32 = *items.first().unwrap() * 10;
        let b: u32 = *items.last().unwrap();
        println!("a: {a}, b: {b}, combined: {}", a + b);
        sum += a + b;
    }
    println!("{:?}", sum)
}
