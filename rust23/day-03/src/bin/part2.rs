use std::fs::File;
use std::io::{BufRead, BufReader};

use itertools::{multizip, Itertools};

fn main() {
    let file = File::open("./input/part2.txt").unwrap();
    let reader = BufReader::new(file);

    let sum: u32 = reader
        .lines()
        .flatten()
        .map(|x| ".".to_string() + &x + ".")
        .tuple_windows::<(String, String, String)>()
        // .inspect(|(x1, x2, x3)| println!("Segment: \n{x1}\n{x2}\n{x3}"))
        .inspect(|_| println!("Segment"))
        .map(process_middle_segment)
        .sum();

    println!("{:?}", sum)
}

/// Note, this operates on the middle string, and uses the others
/// for comparison
fn process_middle_segment(input: (String, String, String)) -> u32 {
    let l = input; // l = line
    let mut sum = 0;
    let mut main_buffer: Vec<u32> = vec![];
    let mut b = ("".to_owned(), "".to_owned(), "".to_owned()); // b = buffer
    let mut s = (false, false, false); // star buffer
    let mut star_found = false;
    for c in multizip((l.0.chars(), l.1.chars(), l.2.chars())) {
        // c = char

        star_found = c.1 == '*';

        if star_found {
            s = (true, true, true);
        }

        if c.0.is_numeric() {
            b.0.push(c.0);
        } else if !c.0.is_numeric() {
            if s.0 && b.0 != "" {
                main_buffer.push(b.0.parse().unwrap());
            }
            b.0 = "".to_owned();
        }

        if c.1.is_numeric() {
            b.1.push(c.1);
        } else if !c.1.is_numeric() {
            if s.1 && b.1 != "" {
                main_buffer.push(b.1.parse().unwrap());
            }
            b.1 = "".to_owned();
        }

        if c.2.is_numeric() {
            b.2.push(c.2);
        } else if !c.2.is_numeric() {
            if s.2 && b.2 != "" {
                main_buffer.push(b.2.parse().unwrap());
            }
            b.2 = "".to_owned();
        }

        if !c.0.is_numeric() && !star_found {
            s.0 = false;
        }

        if !c.1.is_numeric() && !star_found {
            s.1 = false;
        }

        if !c.2.is_numeric() && !star_found {
            s.2 = false;
        }

        if s == (false, false, false) && main_buffer.len() == 2 {
            let total = main_buffer[0] * main_buffer[1];
            sum += total;
            println!("{sum:?}");
            main_buffer = vec![];
        } else if s == (false, false, false) {
            main_buffer = vec![];
        }

        if star_found {
            s = (true, true, true);
        }

        println!("{} {} {} {s:5?} {b:?} {main_buffer:?}", c.0, c.1, c.2)
    }

    sum
}
