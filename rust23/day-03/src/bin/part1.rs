use std::fs::File;
use std::io::{BufRead, BufReader};

use itertools::{multizip, Itertools};

fn main() {
    let file = File::open("./input/part1.txt").unwrap();
    let reader = BufReader::new(file);

    let sum: u32 = reader
        .lines()
        .flatten()
        .map(|x| ".".to_string() + &x + ".")
        .tuple_windows::<(String, String, String)>()
        .inspect(|(x1, x2, x3)| println!("Segment: \n{x1}\n{x2}\n{x3}"))
        .map(process_middle_segment)
        .sum();

    println!("{:?}", sum)
}

/// Note, this operates on the middle string, and uses the others
/// for comparison
fn process_middle_segment(input: (String, String, String)) -> u32 {
    let (l1, l2, l3) = input;
    let mut current_symbol = false;
    let mut symbol_buffer = false;
    let mut numbers: Vec<u32> = vec![];
    let mut number: String = String::new();
    for (c1, c2, c3) in multizip((l1.chars(), l2.chars(), l3.chars())) {
        current_symbol = (c1 != '.' && !c1.is_numeric())
            || (c2 != '.' && !c2.is_numeric())
            || (c3 != '.' && !c3.is_numeric());

        if number.is_empty() && !current_symbol && !c2.is_numeric() {
            symbol_buffer = false;
        }

        if current_symbol {
            symbol_buffer = true;
        }

        print!("{c1} {c2} {c3}  {current_symbol:5} {symbol_buffer:5} {number:3}");
        if c2.is_numeric() {
            number.push(c2);
        } else if !number.is_empty() && symbol_buffer {
            numbers.push(number.parse().unwrap());
            // numbers += number.parse::<u32>().unwrap();
            number = "".to_owned();
            symbol_buffer = current_symbol;
        } else {
            number = "".to_owned();
        }

        println!(" {numbers:?}")
    }
    numbers.iter().sum()
}
