use std::fs::File;
use std::io::{BufRead, BufReader};

use itertools::Itertools;

fn main() {
    let file = File::open("./input/part2.txt").unwrap();
    let reader = BufReader::new(file);

    let sum: u32 = reader
        .lines()
        .flatten()
        .map(|x| x.split(&[';', ':']).map(|x| x.to_owned()).collect())
        .map(|x: Vec<String>| {
            let mut game = x.into_iter();
            let id: u32 = game
                .next()
                .unwrap()
                .split(" ")
                .skip(1)
                .next()
                .unwrap()
                .parse()
                .unwrap();
            let mut max_colours = (0, 0, 0); // RGB
            for turn in game {
                for colour_pair in turn.split(",").collect::<Vec<&str>>() {
                    let mut colour_pair = colour_pair.split(" ").skip(1);
                    let number: u32 = colour_pair.next().unwrap().parse().unwrap();
                    let colour = colour_pair.next().unwrap();

                    match colour {
                        "red" => if max_colours.0 < number {max_colours.0 = number},
                        "green" => if max_colours.1 < number {max_colours.1 = number},
                        "blue" => if max_colours.2 < number {max_colours.2 = number},
                        _ => panic!(),
                    }
                }
            }
            (id, max_colours)
        })
        .inspect(|x| println!("Possible Game: {x:?}"))
        .map(|(_, x)| x.0 * x.1 * x.2)
        .sum();
    println!("{:?}", sum)
}
