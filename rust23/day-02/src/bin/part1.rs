use std::fs::File;
use std::io::{BufRead, BufReader};

use itertools::Itertools;

fn main() {
    let file = File::open("./input/part2.txt").unwrap();
    let reader = BufReader::new(file);

    let sum: u32 = reader
        .lines()
        .flatten()
        .map(|x| x.split(&[';', ':']).map(|x| x.to_owned()).collect())
        .map(|x: Vec<String>| {
            let mut game = x.into_iter();
            let id: u32 = game
                .next()
                .unwrap()
                .split(" ")
                .skip(1)
                .next()
                .unwrap()
                .parse()
                .unwrap();
            let mut turns: Vec<(u32, u32, u32)> = vec![];
            for turn in game {
                let mut tuple_turn = (0, 0, 0); // RGB
                for colour_pair in turn.split(",").collect::<Vec<&str>>() {
                    let mut colour_pair = colour_pair.split(" ").skip(1);
                    let number: u32 = colour_pair.next().unwrap().parse().unwrap();
                    let colour = colour_pair.next().unwrap();

                    match colour {
                        "red" => tuple_turn.0 = number,
                        "green" => tuple_turn.1 = number,
                        "blue" => tuple_turn.2 = number,
                        _ => panic!(),
                    }
                }
                turns.push(tuple_turn);
            }
            (id, turns)
        })
        .flat_map(|x| {
            let mut result = vec![];
            for value in x.1.into_iter() {
                println!("{:?}", value);
                if !validate(&value) {
                    println!("Game not possible");
                    return None;
                } else {
                    result.push(value)
                }
            }
            Some((
                x.0,
                result,
            ))
        })
        .inspect(|x| println!("Possible Game: {x:?}"))
        .map(|x| x.0)
        .sum();
    println!("{:?}", sum)
}

fn validate(turn: &(u32, u32, u32)) -> bool {
    turn.0 <= 12 && turn.1 <= 13 && turn.2 <= 14
}
