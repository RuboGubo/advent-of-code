def P(k, n):
    result = 0
    for r in range(1, n + 1):
        result += r**k
    return result

def is_integer(n):
    return n == int(n)

i = 1
while True:

    if is_integer(P(2, i)/P(1, i)) and is_integer(P(4, i)/P(1, i)) and is_integer(P(4, i)/P(2, i)):
        print(P(2, i)/P(1, i) + P(4, i)/P(1, i) + P(4, i)/P(2, i))
        print(i)

    i += 1


