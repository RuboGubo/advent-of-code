use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::Deref;

use itertools::{multizip, Itertools};

fn main() {
    let file = File::open("./input/part2.txt").unwrap();
    let reader = BufReader::new(file);

    let sum: u32 = reader
        .lines()
        .flatten()
        .flat_map(|x| x.split(":").skip(1).map(str::to_owned).next())
        .inspect(|x| println!("{x:?}"))
        .map(find_value)
        .inspect(|x| println!("Segment: {x}"))
        .sum();

    println!("{:?}", sum)
}

fn find_value(ticket: String) -> u32 {
    let mut set_iter = ticket.split("|").map(|x| {
        let numbers: HashSet<u32> = x.split(" ").flat_map(str::parse::<u32>).collect();
        numbers
    });
    let a = set_iter.next().unwrap();
    let b = set_iter.next().unwrap();

    let c: Vec<u32> = a.intersection(&b).map(|x| *x).collect();
    let c: usize = c.len();

    println!("{c}");

    if c > 0 {
        2_u32.pow((c - 1) as u32)
    } else {
        0
    }
}
