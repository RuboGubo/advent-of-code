use std::collections::{HashSet, VecDeque};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::repeat;

use itertools::Itertools;

fn main() {
    let file = File::open("./input/part2.txt").unwrap();
    let reader = BufReader::new(file);

    let mut queue: VecDeque<u32> = VecDeque::new();

    println!("{queue:?}");

    let sum: u32 = reader
        .lines()
        .flatten()
        .flat_map(|x| x.split(":").skip(1).map(str::to_owned).next())
        .inspect(|x| println!("{x:?}"))
        .map(|ticket| find_value(ticket, &mut queue))
        .inspect(|x| println!("Segment: {x}"))
        .sum();

    println!("{:?}", sum)
}

fn find_value(ticket: String, queue: &mut VecDeque<u32>) -> u32 {
    let mut set_iter = ticket.split("|").map(|x| {
        let numbers: HashSet<u32> = x.split(" ").flat_map(str::parse::<u32>).collect();
        numbers
    });
    let a = set_iter.next().unwrap();
    let b = set_iter.next().unwrap();

    let number_of_current_ticket = queue.pop_front().unwrap_or(0) + 1;

    let extra_tickets: Vec<u32> = a.intersection(&b).map(|_| number_of_current_ticket).collect();
    *queue = merge_lists(queue, &extra_tickets).into();
    println!("{extra_tickets:?} {queue:?}");

    number_of_current_ticket
}

fn merge_lists(list_a: &mut VecDeque<u32>, list_b: &Vec<u32>) -> Vec<u32> {
    list_a
        .iter()
        .zip_longest(list_b)
        .map(|x| {
            use itertools::EitherOrBoth as E;
            match x {
                E::Both(x, y) => x + y,
                E::Left(x) => *x,
                E::Right(x) => *x,
            }
        })
        .collect()
}
