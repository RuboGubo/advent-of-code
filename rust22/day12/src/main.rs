use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

#[derive(Debug, PartialEq)]
enum Cell {
    Normal(u32),
    End,
    Start,
}

impl Cell {
    pub fn new(hight: u32) -> Self {
        Self::Normal(hight)
    }

    pub fn get_value(&self) -> u32 {
        match self {
            Cell::Normal(x) => *x,
            Cell::End => 26,
            Cell::Start => 0,
        }
    }
}

impl From<char> for Cell {
    fn from(letter: char) -> Self {
        let alphabet: Vec<char> = ('a'..='z').collect();

        if letter == 'E' {
            Cell::End
        } else if letter == 'S' {
            Cell::Start
        } else {
            let re: u32 = alphabet
                .iter()
                .position(|x| x == &letter)
                .expect("Not a valid letter?") as u32;

            Cell::new(re)
        }
    }
}

#[derive(Debug, Eq, Hash, PartialEq, Clone, Copy)]
struct Cord {
    x: usize,
    y: usize,
}

impl Cord {
    pub fn new(x: usize, y: usize) -> Self {
        Self { x, y }
    }

    pub fn new_validate(map: &HightMap, x: i32, y: i32) -> Option<Self> {
        if 0 <= x && x < map.horizontal_len() as i32 && 0 <= y && y < map.vertical_len() as i32 {
            Some(Self {
                x: x as usize,
                y: y as usize,
            })
        } else {
            None
        }
    }
}

impl Cord {
    pub fn get_around_cords(&self, map: &HightMap) -> Vec<Self> {
        let x = self.x as i32;
        let y = self.y as i32;
        let cords: Vec<Option<Self>> = vec![
            Self::new_validate(map, x + 1, y),
            Self::new_validate(map, x - 1, y),
            Self::new_validate(map, x, y + 1),
            Self::new_validate(map, x, y - 1),
        ];
        let mut real_cords = vec![];
        cords.into_iter().flatten().for_each(|x| real_cords.push(x));

        real_cords
    }
}

#[derive(Debug, Default)]
struct HightMap {
    map: Vec<Vec<Cell>>,
}

impl HightMap {
    pub fn new(map: Vec<Vec<Cell>>) -> Self {
        Self { map }
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> Self {
        let file = File::open(path).expect("File does not exist?");
        let reader = BufReader::new(file);

        let map: Vec<Vec<Cell>> = reader
            .lines()
            .map(|round| {
                round
                    .expect("not a round?")
                    .chars()
                    .map(|letter| letter.into())
                    .collect()
            })
            .collect();

        Self::new(map)
    }

    pub fn vertical_len(&self) -> usize {
        self.map.len()
    }

    pub fn horizontal_len(&self) -> usize {
        self.map[0].len()
    }

    pub fn get_cell(&self, cord: &Cord) -> &Cell {
        &self.map[cord.y][cord.x]
    }

    fn find(&self, cell: Cell) -> Option<Cord> {
        for (y, row) in self.map.iter().enumerate() {
            for (x, column) in row.iter().enumerate() {
                if *column == cell {
                    return Some(Cord::new(x, y));
                }
            }
        }

        None
    }

    pub fn find_start(&self) -> Option<Cord> {
        self.find(Cell::Start)
    }

    pub fn find_end(&self) -> Option<Cord> {
        self.find(Cell::End)
    }
}

#[derive(Debug)]
struct AdjacencyList {
    list: HashMap<Cord, Vec<Cord>>,
}

impl AdjacencyList {
    pub fn from_hight_map(map: &HightMap) -> Self {
        let mut list: HashMap<Cord, Vec<Cord>> = HashMap::new();

        for y in 0..map.vertical_len() {
            for x in 0..map.horizontal_len() {
                // index start 0, len start 1
                let current_cord = Cord::new(x, y);
                let valid_cords: Vec<Cord> = current_cord
                    .get_around_cords(map)
                    .into_iter()
                    .filter(|this_cord| {
                        let this_cell = map.get_cell(this_cord).get_value() as i32;
                        let current_cell = map.get_cell(&current_cord).get_value() as i32;

                        current_cell == this_cell + 1
                            || current_cell == this_cell
                            || current_cell == this_cell - 1
                    })
                    .collect();
                list.insert(current_cord, valid_cords);
            }
        }

        Self { list }
    }

    fn find_distance(&self, start: Cord, end: Cord) -> u32 {
        Self::dijkstra(self, start, end)
    }

    fn dijkstra(list: &AdjacencyList, start: Cord, end: Cord) -> u32 {
        // For now keep the foreign definitions, as it makes it
        // easier to read
        let mut nodes: Vec<Cord> = list.list.keys().map(|x| *x).collect();
        let mut distances: HashMap<Cord, u32> = HashMap::new();
        let mut previous_vertex: HashMap<Cord, Option<Cord>> = HashMap::new();

        for node in nodes.iter() {
            distances.insert(*node, u32::MAX);
            previous_vertex.insert(*node, None);
        }
        distances.insert(start, 0);

        while !nodes.is_empty() {
            let selected_vertex = Self::find_small_dist(&distances, &nodes);

            if let Some(selected_vertex) = selected_vertex {
                if selected_vertex == end {
                    println!("{:?} {:?} {:?}", selected_vertex, start, end);
                    let mut s: Vec<Cord> = vec![];
                    let mut u = Some(end);
                    if previous_vertex.get(&end).is_some() || u == Some(start) {
                        while u.is_some() {
                            s.insert(0, u.unwrap());
                            u = *previous_vertex.get(&u.unwrap()).unwrap();
                        }
                    }

                    for l in &previous_vertex {
                        println!("{:?}", l)
                    }

                    for l in &s {
                        println!("{:?}", l)
                    }

                    return *distances.get(&selected_vertex).unwrap();
                };

                let index = nodes.iter().position(|x| *x == selected_vertex).expect("nodes does not exist");
                nodes.remove(index);

                list.list.get(&selected_vertex).unwrap()
                    .iter()
                    .filter(|neighbor| nodes.contains(&neighbor))
                    .for_each(|neighbor| {
                        let alt_distance = *distances.get(&selected_vertex).unwrap() + 1;
                        let neighbor_distance = *distances.get(neighbor).unwrap();
                        let smaller = alt_distance < neighbor_distance;

                        println!("Neighbor: {:?} {}", &neighbor, smaller);
                        if smaller {
                            distances.insert(*neighbor, alt_distance);
                        }
                    });
            };
        }
        0
    }

    fn find_small_dist(distances: &HashMap<Cord, u32>, nodes: &[Cord]) -> Option<Cord> {
        let mut smallest = (None, &u32::MAX);
        for vertex in distances {
            if vertex.1 <= smallest.1 && nodes.contains(vertex.0) {
                smallest = (Some(vertex.0), vertex.1);
            }
        }
        if smallest.1 == &u32::MAX {
            None
        } else {
            smallest.0.copied()
        }
    }
}

fn print_debug(distances: &HashMap<Cord, u32>) {
    for i in distances {
        println!("{:?}", i)
    }
}

fn main() {
    let hight_map = HightMap::from_file("r_text.txt");
    println!("hight_map Done");

    let adjacency_list = AdjacencyList::from_hight_map(&hight_map);
    println!("adjacency_list Done");

    println!(
        "Distance: {}",
        adjacency_list.find_distance(
            hight_map.find_start().unwrap(),
            hight_map.find_end().unwrap()
        )
    );
}
