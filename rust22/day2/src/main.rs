use std::{fs::File, io::{BufReader, BufRead}};

const WIN_SCORE:u32 = 6;
const DRAW_SCORE: u32 = 3;
const _LOSE_SCORE: u32 = 0;

fn main() {
    let file = File::open("strategy_guide.txt").expect("File does not exist?");
    let reader = BufReader::new(file);

    let mut score:u32 = 0;

    for round in reader.lines() {
        

        score += match round {
            Ok(x) => find_total_score(x.split(" ").collect()),
            Err(_) => 0,
        };
    }

    println!("{}", score)
}

fn find_total_score(round:Vec<&str>) -> u32{
    let mut strat_id:Vec<u32> = vec![];

    for strat in &round {
        strat_id.push(find_score(strat));
    }

    match &strat_id[1] {
        1 => {manage_fake_three(&(strat_id[0]-1))},
        2 => {strat_id[0] + &DRAW_SCORE},
        3 => {manage_fake_three(&(strat_id[0]+1)) + &WIN_SCORE},
        _ => panic!("WHAT DID YOU DOOO")
    }
}

fn manage_fake_three(strat_id:&u32) -> u32 {
    let value = (strat_id) % 3;
    if value == 0 {
        3
    } else if value == 4{
        1
    } else {
        value
    }
}

fn find_score(code:&str) -> u32 {
    match code {
        "A" => 1, // Rock
        "B" => 2, // Paper
        "C" => 3, // Scissors
        "X" => 1, // Lose
        "Y" => 2, // Draw
        "Z" => 3, // Win
        _ => panic!("THAT IS NOT A CODE YOU IDIOT!!!!!!!!!!!1")
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn win_a() {
        let input: Vec<&str> = vec!["A", "Z"];

        let r = find_total_score(input);
        assert_eq!(r, 8)
    }

    #[test]
    fn win_b() {
        let input: Vec<&str> = vec!["B", "Z"];

        let r = find_total_score(input);
        assert_eq!(r, 9)
    }

    #[test]
    fn win_c() {
        let input: Vec<&str> = vec!["C", "Z"];

        let r = find_total_score(input);
        assert_eq!(r, 7)
    }

    #[test]
    fn lose_a() {
        let input: Vec<&str> = vec!["A", "X"];

        let r = find_total_score(input);
        assert_eq!(r, 3)
    }

    #[test]
    fn lose_b() {
        let input: Vec<&str> = vec!["B", "X"];

        let r = find_total_score(input);
        assert_eq!(r, 1)
    }

    #[test]
    fn draw_a() {
        let input: Vec<&str> = vec!["A", "Y"];

        let r = find_total_score(input);
        assert_eq!(r, 4)
    }
}