use std::{
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Debug)]
enum CommandType {
    Cd(String),
    Ls,
}

#[derive(Debug)]
enum DataType {
    Command(CommandType),
    Directory(String),
    File(u32),
}

struct Directory {
    name: String,
    size: u32,
    child_directories: Vec<Directory>,
}

impl Directory {
    fn execute_command(&mut self, command: CommandType) -> &mut Self {
        match command {
            CommandType::Cd(name) => self.execute_cd(&name),
            CommandType::Ls => todo!(),
        }
    }

    fn execute_cd(&mut self, name: &str) -> &mut Self {
        if self.name == name {
            self
        } else {
            let child = self.find_in_child_dir(&name);
            match child {
                Some(x) => x.execute_cd(&name),
                None => self.create_child_dir(&name),
            }
        }
    }

    fn find_in_child_dir(&mut self, name: &str) -> Option<&mut Directory> {
        self.child_directories.iter_mut().find(|x| x.name == name)
    }

    fn create_child_dir(&mut self, name: &str) -> &mut Self {
        let new_dir = Self::new(name);
        self.child_directories.push(new_dir);
        self.child_directories.last_mut().unwrap()
    }
}

impl Directory {
    fn new(name: &str) -> Self {
        Self {
            name: name.to_owned(),
            size: 0,
            child_directories: vec![],
        }

    }
}

fn main() {
    let file = File::open("source.txt").expect("File does not exist?");
    let reader = BufReader::new(file);

    let mut current_dir: &mut Directory = &mut Directory {
        name: "/".to_owned(),
        size: 0,
        child_directories: vec![],
    };

    for command in reader.lines() {
        let command = parse_data(command.unwrap());
        println!("{:?}", command);

        current_dir = match command {
            DataType::Command(x) => current_dir.execute_command(x),
            DataType::Directory(_) => todo!(),
            DataType::File(_) => todo!(),
        }
    }
}

fn parse_data(line: String) -> DataType {
    if line.contains('$') {
        DataType::Command(parse_command(line).unwrap())
    } else if line.contains("dir") {
        DataType::Directory(line.replace("dir ", ""))
    } else {
        DataType::File(line.split(' ').collect::<Vec<_>>()[0].parse().unwrap())
    }
}

fn parse_command(line: String) -> Option<CommandType> {
    if line.contains("cd") {
        Some(CommandType::Cd(line.replace("$ cd ", "")))
    } else if line.contains("ls") {
        Some(CommandType::Ls)
    } else {
        None
    }
}
