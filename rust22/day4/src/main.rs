use std::{
    fs::File,
    io::{BufRead, BufReader},
    ops::RangeInclusive,
};

fn main() {
    let file = File::open("source.txt").expect("File does not exist?");
    let reader = BufReader::new(file);

    let mut score: u32 = 0;

    for round in reader.lines() {
        score += match round {
            Ok(x) => parse_line(x),
            Err(_) => 0,
        };
    }

    println!("{}", score)
}

fn parse_line(line: String) -> u32 {
    let ranges: Vec<&str> = line.split(",").collect();

    let range_a: Vec<&str> = ranges[0].split("-").collect();
    let range_a = range_a[0].parse::<u32>().unwrap()..=range_a[1].parse::<u32>().unwrap();

    let range_b: Vec<&str> = ranges[1].split("-").collect();
    let range_b = range_b[0].parse::<u32>().unwrap()..=range_b[1].parse::<u32>().unwrap();

    check_if_in_range(range_a, range_b).try_into().unwrap()
}

fn check_if_in_range(range_a: RangeInclusive<u32>, range_b: RangeInclusive<u32>) -> bool {
       range_a.contains(&range_b.start()) 
    || range_a.contains(&range_b.end())
    || range_b.contains(&range_a.start()) 
    || range_b.contains(&range_a.end())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn contains_test_1() {
        let range_a = 2..=8;
        let range_b = 3..=7;

        let r = check_if_in_range(range_a, range_b);
        assert!(r)
    }

    #[test]
    fn contains_test_2() {
        let range_a = 6..=6;
        let range_b = 4..=6;

        let r = check_if_in_range(range_a, range_b);
        assert!(r)
    }

    #[test]
    fn overlap_test_1() {
        let range_a = 5..=7;
        let range_b = 7..=9;

        let r = check_if_in_range(range_a, range_b);
        assert!(r)
    }

    #[test]
    fn overlap_test_2() {
        let range_a = 2..=6;
        let range_b = 4..=8;

        let r = check_if_in_range(range_a, range_b);
        assert!(r)
    }
}
