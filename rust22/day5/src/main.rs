use std::{fs::File, io::{BufReader, BufRead}};

fn main() {
    let file = File::open("source.txt").expect("File does not exist?");
    let reader = BufReader::new(file).lines();

    let mut stacks = MainArea::new();
    let mut parsing_stack:bool = true;

    for round in reader {
        

        let round = round.unwrap();
        
        if parsing_stack {
            stacks.load_stacks(round.clone());
        } else {
            stacks.run_command(round.clone());
        }
        
        if round == "".to_owned(){
            parsing_stack = false;
        }
    }
    dbg!(&stacks);
}

#[derive(Debug)]
struct MainArea {
    stacks:Vec<Vec<char>>
}

impl MainArea {
    fn new() -> Self {
        Self {
            stacks: vec![vec![]; 9]
        }
    }

    fn load_stacks(&mut self, round:String) {
        let values: Vec<char> = round.chars()
                                     .skip(1)
                                     .step_by(4)
                                     .collect();



        for (index, value) in values.iter().enumerate() {
            if value != &' ' {
                self.stacks[index].push(*value);
            }
        }
    }

    fn run_command(&mut self, round:String) {
        let command  = self.command_parser(round);

        let iterations = &(command[0] as usize);
        let departure_stack = (&command[1]-1) as usize;
        let destination_stack = (&command[2]-1) as usize;

        
        let start_stack = self.stacks.get_mut(departure_stack).expect("oh no, that is not a correct index");
        let value:Vec<_> = start_stack.drain(0..*iterations).collect();

        let end_stack = self.stacks.get_mut(destination_stack).expect("oh no, that is not a correct index");
        for item in value.iter().rev() {
            end_stack.insert(0, *item);
        }

    }

    fn command_parser(&self, round:String) -> Vec<u32> {
        round.split(" ")
             .skip(1)
             .step_by(2)
             .map(|x| x.parse::<u32>().unwrap())
             .collect()
    }
}

impl std::fmt::Display for MainArea {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:#?}", self.stacks)
    }
}
