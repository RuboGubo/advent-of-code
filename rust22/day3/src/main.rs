use std::{fs::File, io::{BufReader, BufRead}, ops::Add, collections::HashSet};

fn main() {
    let file = File::open("source.txt").expect("File does not exist?");
    let reader = BufReader::new(file);

    let mut score:u32 = 0;
    let mut lines: Vec<Vec<char>> = vec![];
    let mut i:u32 = 0;

    for round in reader.lines() {
        
        match round {
            Ok(x) => lines.push(x.chars().collect()),
            Err(_) => (),
        };
        i += 1;
        if i % 3 == 0 {   
            score += rucksack_main(&lines);
            lines = vec![];
        }
    }

    println!("{}", score)
}

fn rucksack_main(rucksack: &Vec<Vec<char>>) -> u32 {
    let first_half = &rucksack[0];
    let second_half = &rucksack[1];
    let third_half = &rucksack[2];

    let result:Vec<&char> = first_half.iter()
                                      .filter(|x| second_half.contains(x))
                                      .filter(|x| third_half.contains(x))
                                      .collect();
    let de_dupe = result
    .into_iter()
    .collect::<HashSet<_>>()
    .into_iter()
    .collect::<Vec<_>>();
    if de_dupe.len() == 1 {
        map_numb_to_letter(de_dupe.get(0).unwrap())
    } else {
        0
    }
}

fn map_numb_to_letter(letter:&char) -> u32 {
    let letters = vec!['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    let uppercase_letters:Vec<char> = vec!['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    let uppercase_letter = letter.to_uppercase().collect::<Vec<char>>()[0];

    let mut value = match letters.iter()
                 .position(|&x| x == *letter) 
        {
            Some(x) => x.try_into().unwrap(),
            None => uppercase_letters
                    .iter()
                    .position(|&x| x == uppercase_letter)
                    .unwrap()
                    .add(26)
                    .try_into()
                    .unwrap(),
        };

    value += 1; // vectors start at 0
    value
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn group_one () {
        let r = rucksack_main(&vec!["vJrwpWtwJgWrhcsFMMfFFhFp".chars().collect(),
                                     "vJrwpWtwJgWrhcsFMMfFFhFp".chars().collect(), 
                                     "vJrwpWtwJgWrhcsFMMfFFhFp".chars().collect()]);

        assert_eq!(r, 18);
    }

}