use std::{fs::File, io::Read, collections::HashSet};

fn main() {
    let mut stream:String = String::new();
    File::open("source.txt").expect("File does not exist?")
                            .read_to_string(&mut stream).unwrap();

    let stream:Vec<_> = stream.chars().enumerate().collect();

    let window_size = 14;

    for window in stream.windows(window_size) {
        let set:HashSet<_> = Vec::from(window).drain(..).map(|x| x.1).collect();
        if set.len() == window_size {
            let index = window.last().unwrap().0;
            dbg!(window.last().unwrap().0);
            dbg!(index+1);

            break;
        }
    }
}
